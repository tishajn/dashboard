import React, {useEffect,useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useFormik, FieldProps, Formik, Form  } from 'formik';
import * as Yup from 'yup';
import * as actions from '../action';
import b64toBlob from 'b64-to-blob';
import { useUserDispatch, useUserState } from "../UserContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errorMsg: {
      color: "red"
  }
}));
const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/png"
];
const validationSchema = Yup.object({
    name: Yup.string().required('Required!'),
    email: Yup.string().email('Invalid email address').required('Required!'),
    password: Yup.string().required('Required!').min(6),
    profilePic: Yup
        .mixed()
        .test(
            "fileFormat",
            "Unsupported Format",
            value => value && SUPPORTED_FORMATS.includes(value.type)
        )
})

const initialValues = {
    name: '',
    profilePic: '',
    dob: '',
    email: '',
    password: '',
}

export default function SignIn(props) {
  const classes = useStyles();
  const userDispatch = useUserDispatch();
  const [error, setError] = useState('');
  let UserState = useUserState();
  const onSubmit = async(values) => {
    let reader = new FileReader();
    reader.readAsDataURL(values.profilePic);
    reader.onload = () => {
        let url = reader.result;
        values.base64 = url
        
    }
    actions.signup(values, userDispatch, props.history)
}

    useEffect(() => {        
        if(UserState.errorMsg)
            setError(UserState.errorMsg);
    }, [UserState]);

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
            Sign Up
            </Typography>
            <p>{error}</p>
            <Formik validationSchema={validationSchema} initialValues={initialValues} onSubmit={onSubmit}>
                {props => {
                    const { handleChange, values, errors, handleSubmit, setFieldValue, touched, getFieldProps, handleBlur  } = props;
                    return (
                        <Form className={classes.form} >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                label="Name"
                                name="name"
                                {... getFieldProps('name')}
                            />
                            {touched.name && errors.name ? (<div className={classes.errorMsg}>{errors.name}</div>): null}
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                {... getFieldProps('email')}
                            />
                            {touched.email && errors.email ? (<div className={classes.errorMsg}>{errors.email}</div>): null}
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                {... getFieldProps('password')}

                            />
                            {touched.password && errors.password ? (<div className={classes.errorMsg}>{errors.password}</div>): null}

                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="dob"
                                label=" "
                                type="date"
                                id="dob"
                                {... getFieldProps('dob')}
                            />
                            
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="profilePic"
                                label="Profile Pic"
                                type="file"
                                id="profilePic"
                                inputProps={{ accept: 'image/x-png, image/gif, image/jpeg' }}
                                onBlur={handleBlur}
                                onChange = {(event) => {
                                    setFieldValue("profilePic", event.currentTarget.files[0])}}
                            />
                            {touched.profilePic && errors.profilePic ? (<div className={classes.errorMsg}>{errors.profilePic}</div>): null}
                            
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                            
                                Sign Up
                            </Button>                            
                        </Form>
                    );
                }}
            </Formik>
        </div>
        </Container>
    );
}