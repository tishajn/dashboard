import React from 'react';
import { useUserDispatch, useUserState } from "../UserContext";
import Button from '@material-ui/core/Button';
import * as action from '../action'

export default function Dashboard (props) {
    const userDispatch = useUserDispatch();
    let UserState = useUserState();
    
    return(
        <>
        <p>Hello {UserState.name}</p>
        <Button variant="contained" color="primary" onClick={() => action.signout(userDispatch, props.history)}>Sign Out</Button>
        </>
    )
}