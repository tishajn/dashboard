import React, {useState, useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useFormik, FieldProps, Formik, Form  } from 'formik';
import * as Yup from 'yup';
import * as actions from '../action';
import { useUserDispatch, useUserState } from "../UserContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid email address').required('Required!'),
        password: Yup.string().required('Required!').min(6),
        
    })

    const initialValues = {
        email: '',
        password: '',
    }

    export default function Login(props) {
    const classes = useStyles();
    const userDispatch = useUserDispatch();
    let UserState = useUserState();
    const [error, setError] = useState('');
    
    const onSubmit = async(values) => {
        actions.login(values, userDispatch, props.history)
    }
    useEffect(() => {        
        if(UserState.errorMsg)
            setError(UserState.errorMsg);
    }, [UserState]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <p>{error}</p>
        
        <Formik validationSchema={validationSchema} initialValues={initialValues} onSubmit={onSubmit}>
            {props => {
                const { handleChange, values, errors, handleSubmit, setFieldValue, touched, getFieldProps, handleBlur  } = props;
                return (
                    <Form className={classes.form} >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            {... getFieldProps('email')}
                        />
                        {touched.email && errors.email ? (<div className={classes.errorMsg}>{errors.email}</div>): null}
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            {... getFieldProps('password')}

                        />
                        {touched.password && errors.password ? (<div className={classes.errorMsg}>{errors.password}</div>): null}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                        
                            Sign In
                        </Button>                        
                    </Form>
                );
            }}
        </Formik>
      </div>
    </Container>
  );
}