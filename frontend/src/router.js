import React from 'react';
import { Route, Redirect } from 'react-router-dom'
import SignUp from './screens/singupForm';
import Login from './screens/login';
import Dashboard from './screens/dashboard';
import { useUserState } from './UserContext'

import App from './App';

export default function Routes() {
	var { isAuthenticated, token } = useUserState();
	return (
		<App>
			<PublicRoute path="/signup" exact component={SignUp} />
			<PublicRoute path="/login" exact component={Login} />
			<PrivateRoute path="/dashboard" exact component={Dashboard} />
			<PrivateRoute path="/" exact component={Dashboard} />
			
		</App>
	);
	// #######################################################################

function PrivateRoute({ component, ...rest }) {
    
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated ? (
            React.createElement(component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: {
                  from: props.location,
                },
              }}
            />
          )
        }
      />
    );
  }

  function PublicRoute({ component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated ? (
            <Redirect
              to={{
                pathname: "/dashboard",
              }}
            />
          ) : (
            React.createElement(component, props)
          )
        }
      />
    );
  }
}

