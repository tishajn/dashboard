import axios from "axios";


export function login(data, dispatch, history) {
    return axios.post(`http://localhost:8080/login`, data)
        .then((res) => {
            console.log(res)
            const user = res.data
            
            dispatch({ type: 'LOGIN_SUCCESS', user })
            localStorage.setItem('token', 1)
            history.push('/dashboard')
        })
        .catch((err) => {
            dispatch({ type: 'LOGIN_FAILURE' } )
        })
}

export function signup(data,  dispatch, history) {
    return axios.post(`http://localhost:8080/signup`, data)
        .then((res) => {
            console.log(res, "res");
            const user = res.data
            dispatch({ type: 'LOGIN_SUCCESS',  user})
            
            localStorage.setItem('token', 1)

            history.push('/dashboard')
        })
        .catch((err) => {
            console.log(err, "sdfsd");
            
            dispatch({ type: 'LOGIN_FAILURE' })
        })
}

export function signout(dispatch, history) {
  
    dispatch({ type: 'SIGN_OUT_SUCCESS'})
    localStorage.removeItem('token')
    history.push('/login')
       
}