const db = require('../Schema/users');
const Bcrypt = require("bcryptjs");

module.exports.getEmailById = (email, password) => new Promise((resolve, reject) => {
    db.findOne({email: email})
    .then((user) => {
        if(!user) {
            return reject("The username does not exist" );
        }
        if(!Bcrypt.compareSync(password, user.password)) {
            return reject("The password is invalid" );
        }
        return resolve(user);
    })
    .catch((error) => {        
        reject(error)
    })
});

module.exports.createUser = (data) => new Promise( (resolve, reject) => {
    data.password = Bcrypt.hashSync(data.password, 10);
    db.findOne({email: data.email})
    .then((user) => {
        if(user) {
            return reject('Email id already in use')
        } 
    })
    db.create(data)
    .then((responseData) => {
        resolve(responseData);
    })
    .catch((err) => {
        reject(err);
    })
});