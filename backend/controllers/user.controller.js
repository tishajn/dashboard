
const constants = require('../constant.json');
const userService = require('../services/user.service')

exports.login = (req, res) => {
    userService.login(req.body)
    .then((user) => {
        res.status(constants.HttpStatus.OK);
        res.send(user);
    })
    .catch((error) => {
        res.status(constants.HttpStatus.INTERNAL_SERVER_ERROR);
        res.send({message: error});
    });

};

exports.signup = (req, res) => {
    userService.signup(req.body)
        .then((user) => {
            res.status(constants.HttpStatus.OK);
            res.send(user);
        })
        .catch((error) => {
            res.status(constants.HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(error);
        });
};