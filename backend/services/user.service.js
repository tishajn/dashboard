const userRepo = require('../repositories/user.repository')
var fs = require("fs");


module.exports.login = (data) => new Promise((resolve, reject) => {
    userRepo.getEmailById(data.email, data.password) 
    .then((user) => {
        resolve(user);
    })
    .catch((err) => {        
        reject(err);
    })
});

module.exports.signup = (data) => new Promise((resolve, reject) => {
    
    if(data.profilePic) {
        data.profilePic = data.base64;
        delete data.base64
    }
    userRepo.createUser(data) 
    .then((result) => {
        resolve(result);
    })
    .catch((err) => {        
        reject(err);
    })
});