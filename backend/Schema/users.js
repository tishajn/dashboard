const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let user = new Schema(
  {
    name: {
      type: String
    },
    email: {
      type: String
    },
    dob: {
      type: String
    },
    password: {
      type: String
    },
    profilePic: {
      type: String
    }
  },
  { collection: "Users" }
);

module.exports = mongoose.model("user", user);