
const express = require('express');
// const graphqlHTTP = require('express-graphql');

const mongo = require('mongoose');
const app = express();
const path = require('path');

const routes = require('./routes');
const userCtrl = require('./controllers/user.controller')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors')

mongo.connect('mongodb+srv://admin:admin0000@userdashboard-wmdys.mongodb.net/dashboard?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongo.connection.once('open', () => {
    console.log('connected to database');
})

app.use(cors())

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

process.env.TZ = 'UTC'; // To set the node timeZone into UTC

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Authorization, Content-Type, access-control-allow-origin');
    next();
})

app.use('/signup', userCtrl.signup);
app.use('/login', userCtrl.login);


/// catch 404 and forwarding to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    // render the error page
    res.status(err.status || 500);
    res.send('Something Went Wrong');
});
// app.use('/graphiql', graphqlHTTP({ schema: require('./schema.js'), graphiql: true}));

app.listen(8080, () => {
    console.log('Server running succefully...') 
})